{
  description = "Diff for flake.lock";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };
  outputs = {
    self,
    flake-utils,
    nixpkgs,
    poetry2nix,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      inherit (poetry2nix.legacyPackages.${system}) mkPoetryApplication mkPoetryEnv;
      selfPkgs = self.packages.${system};
    in {
      packages = {
        flake-lock-diff = mkPoetryApplication {
          projectDir = ./.;
        };
        default = selfPkgs.flake-lock-diff;
      };

      devShells.default = pkgs.mkShell {
        packages = [
          poetry2nix.packages.${system}.poetry
          self.shells.${system}.default
          pkgs.pyright
        ];
      };

      shells.default = mkPoetryEnv {
        projectDir = ./.;
        extraPackages = pp: [pp.ipython];
      };
    });
}
