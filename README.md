# Nix flake.lock diff

WIP diff to print differences between two `flake.lock` files (currently only
handles github inputs).

It can be used as git's external diff:

    $ nix flake update
    $ GIT_EXTERNAL_DIFF='nix run gitlab:psentee/flake-lock-diff -- --git' git diff -- flake.lock
