#!/usr/bin/env python3
from __future__ import annotations

import json
import urllib.parse
import sys
from os import PathLike
from typing import Any


def todo(*args: Any):
    print("TODO:", *args, file=sys.stderr)


class FlakeLock:
    nodes: dict[str, Node]
    _root: str
    version: int

    def __init__(self, root: str, version: int, nodes: dict[str, dict[str, Any]]):
        assert root in nodes
        self._root = root
        self.version = version
        self.nodes = {}
        for name, node in nodes.items():
            if "flake" not in node:
                node["flake"] = None
            if "inputs" not in node:
                node["inputs"] = {}
            self.nodes[name] = Node(self, **node)

    @classmethod
    def load(
        cls, path: int | str | bytes | PathLike[str] | PathLike[bytes]
    ) -> FlakeLock:
        with open(path) as f:
            data = json.load(f)
        return cls(data["root"], data["version"], data["nodes"])

    def __getitem__(self, k: str) -> Node:
        return self.nodes[k]

    @property
    def root(self) -> Node:
        return self[self._root]

    def print(self):
        self.root.print(prefix=self._root)

    def diff(self, other: FlakeLock):
        self.root.diff(other.root)


class Node:
    _lock: FlakeLock
    inputs: dict[str, str | tuple[str]]
    flake: bool | None
    original: dict[str, Any]
    locked: dict[str, Any]

    def __init__(
        self,
        lock: FlakeLock,
        *,
        original: dict[str, Any] | None = None,
        locked: dict[str, Any] | None = None,
        inputs: dict[str, str | tuple[str]] | None = None,
        flake: bool | None = None,
    ):
        self._lock = lock
        self.inputs = inputs or {}
        self.flake = flake
        self.original = original or {}
        self.locked = locked or {}

    @property
    def type(self) -> str | None:
        return self.original.get("type", None)

    @property
    def rev(self) -> str | None:
        return self.locked.get("rev", None)

    def __str__(self) -> str:
        if self.type in {"github", "gitlab"}:
            return f"https://{self.type}.com/{self.original['owner']}/{self.original['repo']}"
        todo(f"URL for {self.type}")
        return urllib.parse.urlencode(self.original)

    def resolve_input(self, iref: str, recursive: bool = True) -> Node | None:
        inp = self.inputs[iref]
        if isinstance(inp, str):
            return self._lock[iref]

        if not recursive:
            return None

        cur = self._lock.root
        for elt in inp:
            cur = cur.resolve_input(elt)
            assert cur is not None
        return cur

    def print(self, *, prefix="$"):
        for name, iref in self.inputs.items():
            qname = f"{prefix}.{name}"
            inp = self.resolve_input(name)
            print(qname, inp)
            if isinstance(iref, str):
                self._lock[iref].print(prefix=qname)
            else:
                print(" `->", ".".join(iref))

    def _resolve_input_ish(self, name: str) -> Node | str | None:
        if name not in self.inputs:
            return None
        ref = self.inputs[name]
        if isinstance(ref, str):
            return self._lock[ref]
        return ".".join(ref)

    def diff(self, other: Node, *, prefix: tuple[str, ...] = ()):
        sprefix = ".".join(prefix)
        if self.original != other.original:
            print(sprefix, f"{self} -> {other}")
        elif self.locked != other.locked:
            args = [sprefix]
            if self.rev and other.rev:
                if self.type == "github":
                    args.append(f"{self}/compare/{self.rev}...{other.rev}")
                elif self.type == "gitlab":
                    args.append(f"{self}/-/compare/{self.rev}...{other.rev}")
                else:
                    todo(f"Diff URL for {self.type}")
                    args.extend((str(self), self.rev, "->", other.rev))
            else:
                todo(f"Details for {self}?")
                args.extend(
                    (
                        urllib.parse.urlencode(self.locked),
                        "->",
                        urllib.parse.urlencode(self.locked),
                    )
                )
            print(*args)

        for name in sorted(set(self.inputs) | set(other.inputs)):
            prefixer = (*prefix, name)
            sprefixer = ".".join(prefixer)

            mine = self._resolve_input_ish(name)
            theirs = other._resolve_input_ish(name)

            assert not (mine is None and theirs is None)

            if theirs is None:
                rev = mine.rev or "?" if isinstance(mine, Node) else mine
                print(sprefixer, rev, "->", "ø")
                continue

            if mine is None:
                rev = theirs.rev or "?" if isinstance(theirs, Node) else theirs
                print(sprefixer, "ø", "->", rev)
                continue

            if isinstance(mine, str):
                if mine != theirs:
                    if isinstance(theirs, str):
                        print(sprefixer, mine, "->", theirs)
                    else:
                        print(sprefixer, mine, "->", theirs, theirs.rev or "?")
                        # TODO: recursive descent just to mark the change in nested inputs
                continue

            if isinstance(theirs, str):
                print(sprefixer, mine, mine.rev or "?", "->", theirs)
                # TODO: recursive descent just to mark the change in nested inputs
                continue

            mine.diff(theirs, prefix=prefixer)


def run():
    import optparse

    parser = optparse.OptionParser()
    parser.add_option("--git", help="Use as git external diff", action="store_true")

    (options, args) = parser.parse_args()
    a = FlakeLock.load(args[0])
    b = FlakeLock.load(args[1])
    if options.git:
        b.diff(a)
    else:
        a.diff(b)


if __name__ == "__main__":
    run()
